 /**
  * @author tang <990921093@qq.com>
  * @description 鼠标拖动实现多选
  */
 
//$(function(){
	 var wId = "w";
	 var index = 0;
	 var startX = 0, startY = 0;
	 var flag = false;
	 var retcLeft = "0px", retcTop = "0px", retcHeight = "0px", retcWidth = "0px";
	 var _retcLeft = 0, _retcTop = 0, _retcHeight = 0, _retcWidth = 0;
	 var create=false;
	 //鼠标按下事件
	 document.onmousedown = function(e){
	 	console.log(2222);
	  	flag = true;
	  	try{
		    var evt = window.event || e;
		    var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
		    var scrollLeft = document.body.scrollLeft || document.documentElement.scrollLeft;
		    startX = evt.clientX + scrollLeft;
		    startY = evt.clientY + scrollTop;
	  	}catch(e){
	  		console.log(e);
	  	}
	 }
	 //鼠标放开事件
	document.onmouseup = function(){
	  	try{
		  	document.body.removeChild(document.getElementById(wId));
		  	
			if(_retcWidth>0){ //反向
				startX=startX-_retcWidth;
			}else{
				_retcWidth=Math.abs(_retcWidth);
			}
			
			if(_retcHeight>0){
				startY=startY-_retcHeight;
			}else{
				_retcHeight=Math.abs(_retcHeight);
			}
			
		  	var seat = $('.seat').each(function(){
		  		var X = $(this).offset().left;
				var Y = $(this).offset().top;
				var width=$(this).width();
				var height=$(this).height();
				
				
				if((X+width)>startX && (Y+height)>startY && X<(startX+_retcWidth) && Y<(startY+_retcHeight)) {
					//$(this).removeClass('selected');
					
					seatsCallback($(this));
					
				}
		  	});
			  
	  	}catch(e){
		    	console.log(e);
	  	}
  		flag = false;
  		create = false;
	}
	 //鼠标移动事件
	 document.onmousemove = function(e){
	  	if(flag){
		   	try{
			   	var evt = window.event || e;
			   	var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
			   	var scrollLeft = document.body.scrollLeft || document.documentElement.scrollLeft;
			   	_retcLeft = (startX - evt.clientX - scrollLeft > 0 ? evt.clientX + scrollLeft : startX);
			   	_retcTop = (startY - evt.clientY - scrollTop > 0 ? evt.clientY + scrollTop : startY);
			   	retcLeft = _retcLeft + "px";
			   	retcTop = _retcTop + "px";
//			   	_retcHeight = Math.abs(startY - evt.clientY - scrollTop);
//			   	_retcWidth = Math.abs(startX - evt.clientX - scrollLeft);
			   	_retcHeight = startY - evt.clientY - scrollTop;
			   	_retcWidth = startX - evt.clientX - scrollLeft;
			   	retcHeight =  Math.abs(_retcHeight) + "px";
			   	retcWidth  =  Math.abs(_retcWidth) + "px";
			  
				if(create==false) {
					if( Math.abs(_retcHeight)>5 ||  Math.abs(_retcWidth)>5) {
					   var div = document.createElement("div");
					   div.id = wId;
					   div.className = "div";
					   div.style.marginLeft = startX + "px";
					   div.style.marginTop = startY + "px";
					   document.body.appendChild(div);
					   create = true;
			   		}
				} else {
					document.getElementById(wId).style.marginLeft = retcLeft;
				    document.getElementById(wId).style.marginTop = retcTop;
				    document.getElementById(wId).style.width = retcWidth;
				    document.getElementById(wId).style.height = retcHeight;
				}
				  	
		   	}catch(e){
		    	//console.log(e);
		   	} 
	  	}
	}

 	
//})